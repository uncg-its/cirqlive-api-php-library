<?php

namespace Uncgits\CirqliveApi;

use Carbon\Carbon;
use GuzzleHttp\Client;

class CirqliveApi {

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */

    private $authMethod; // 'basic', 'digest', 'rfc5849', 'aws', 'cryptoauth'

    private $apiHost; // host and authentication method only, no protocol

    private $username;
    private $password;

    private $proxyHost;
    private $proxyPort;

    private $useProxy = false;

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    /**
     * @param mixed $authMethod
     */
    public function setAuthMethod($authMethod) {
        $this->authMethod = $authMethod;
    }

    /**
     * @param mixed $apiHost
     */
    public function setApiHost($apiHost) {
        $this->apiHost = $apiHost;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    /**
     * @param mixed $proxyHost
     */
    public function setProxyHost($proxyHost)
    {
        $this->proxyHost = $proxyHost;
    }

    /**
     * @param mixed $proxyPort
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * @param boolean $useProxy
     */
    public function setUseProxy($useProxy)
    {
        $this->useProxy = ($useProxy === true);
    }

    /*
    |--------------------------------------------------------------------------
    | API Calls
    |--------------------------------------------------------------------------
    |
    | Methods that actually perform the API calls to CirQlive APIs. Not callable
    | directly; only internally by other class methods.
    |
    */

    /**
     * apiCall() - performs a call to the CirQlive API
     *
     * @param string $function - API function (per CirQlive API docs)
     * @param string $method - Request method (e.g. GET, POST, HEAD, etc.)
     * @param string $params - parameters for the API call (optional)
     * @param string $download - boolean, set to true if the result is a download
     *
     * @return array
     */

    protected function apiCall($function, $method, $params = [], $download = false) {

        $requiredProperties = ['authMethod', 'apiHost', 'username', 'password'];
        foreach ($requiredProperties as $property) {
            if ($this->$property === null) {
                throw new \Exception("Error: required property '$property' has not been set in API object.");
            }
        }

        // assemble the request target and endpoint
        $endpoint = 'https://' . $this->apiHost . '/' . $function;

        // instantiate Guzzle client with basic auth
        // TODO - add other auth options here.
        switch ($this->authMethod) {
            case 'digest':
                $auth = [$this->username, $this->password, 'digest'];
                break;
            case 'basic':
            default:
                $auth = [$this->username, $this->password];
                break;
        }


        $client = new Client([
            'auth' => $auth
        ]);

        // set up basic options
        $requestOptions = [
            'http_errors' => false // don't throw exceptions on HTTP errors
        ];

        // params / body
        if (count($params) > 0 ) {
            if (strtolower($method) == 'get') {
                $requestOptions['query'] = $params;
            } else {
                $requestOptions['form_params'] = $params;
            }
        }

        // use proxy if it is set in .env
        if ($this->useProxy) {
            $requestOptions['proxy'] = $this->proxyHost . ':' . $this->proxyPort;
        }

        // perform the call
        $response = $client->$method($endpoint, $requestOptions);

        // get response data
        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();
        $body = $response->getBody();
        $bodyContents = $body->getContents();

        // parse basic info for all requests

        // TODO - handle downloads.

        // json
        if (!$download) {
            $bodyContents = json_decode($bodyContents);
        }

        // return raw data about the request so that it can be parsed by the calling function
        return array(
            'response' => array(
                'endpoint'    => $function,
                'method'      => $method,
//                'message'     => $servMessage,
                'httpCode'    => $code,
                'httpReason'  => $reason
            ),
            'body'     => $bodyContents
        );
    }


    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    | Methods that perform common operations between other outward-facing
    | methods (e.g. getting a list of users)
    |
    */

    /**
     * checkOptions() - helper function to check provided options against a list of accepted options for an endpoint
     *
     * @param $acceptedOptions
     * @param $options
     *
     * @throws \Exception
     */
    public function checkOptions($acceptedOptions, $options) {
        foreach($options as $option => $value) {
            if (!in_array($option, $acceptedOptions)) {
                throw new \Exception("Error: option '$option' is not valid for this endpoint.");
            }
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    |
    | API methods - note that this will likely expand as the API is fleshed out
    |
    */

    /**
     * listConnections() - lists LTI connections to the CirQlive MEETS platform
     *
     * @return array
     */
    public function listConnections() {
        return $this->apiCall('list_connections', 'get');
    }

    public function listCourses($options = []) {
        $acceptedOptions = [
            'meetsIdConnections',           // comma-separated list, MEETS-internal connection IDs
            'meetsIdUsers_canAccess',       // comma-separated list, MEETS-internal user IDs - show only results that these users can access
            'meetsIdUsers_isAdministrator', // comma-separated list, MEETS-internal user IDs - show only results that these users can access as administrator
            'meetsIdUsers_isTeacher',       // comma-separated list, MEETS-internal user IDs - show only results that these users can access as teacher
            'meetsIdUsers_isStudent',       // comma-separated list, MEETS-internal user IDs - show only results that these users can access as student
        ];

        $this->checkOptions($acceptedOptions, $options);

        return $this->apiCall('list_courses', 'get', $options);
    }

    /**
     * listConferencingEvents() - lists all events created via the MEETS platform. Some options available; see documentation and code comments below for more details.
     *
     * @param array $options
     *
     * @return array
     */
    public function listConferencingEvents($options = []) {
        $acceptedOptions = [
            'meetsIdConnections',           // comma-separated list, MEETS-internal connection IDs
            'meetsIdCourses',               // comma-separated list, MEETS-internal course IDs
            'meetsIdUsers_host',            // comma-separated list, MEETS-internal user IDs
            'meetsIdConferencingAccounts',  // comma-separated list, MEETS-internal conferencing account IDs
            'conferencingServices',         // comma-separated list of conferencing services ('webexmeetingcenter', 'webexeventcenter', 'webextrainingcenter' (other non-WebEx services available in documentation)
            'timeBeginBefore',              // UNIX timestamp
            'timeBeginAfter',               // UNIX timestamp
            'timeEndBefore',                // UNIX timestamp
            'timeEndAfter',                 // UNIX timestamp
        ];

        $this->checkOptions($acceptedOptions, $options);

        return $this->apiCall('list_conferencing_events', 'get', $options);
    }

    /**
     * listConferenceRecordings() - lists all recordings contained in the MEETS platform database. Some options available; see documentation and code comments below for more details.
     *
     * @param array $options
     *
     * @return array
     */
    public function listConferenceRecordings($options = []) {
        $acceptedOptions = [
            'meetsIdConnections',           // comma-separated list, MEETS-internal connection IDs
            'meetsIdCourses',               // comma-separated list, MEETS-internal course IDs
            'meetsIdConferencingEvents',    // comma-separated list, MEETS-internal event IDs
            'meetsIdUsers_host',            // comma-separated list, MEETS-internal user IDs
            'meetsIdConferencingAccounts',  // comma-separated list, MEETS-internal conferencing account IDs
            'type',                         // 'original' (ARF) or 'converted' (MP4)
            'conferencingServices',         // comma-separated list of conferencing services ('webexmeetingcenter', 'webexeventcenter', 'webextrainingcenter' (other non-WebEx services available in documentation)
            'timeAddedBefore',              // UNIX timestamp, based on time added to MEETS database
            'timeAddedAfter',               // UNIX timestamp, based on time added to MEETS database
            'timeEventBeginBefore',         // UNIX timestamp
            'timeEventBeginAfter',          // UNIX timestamp
            'timeEventEndBefore',           // UNIX timestamp
            'timeEventEndAfter',            // UNIX timestamp
        ];

        $this->checkOptions($acceptedOptions, $options);

        return $this->apiCall('list_conference_recordings', 'get', $options);
    }

    /**
     * getConferencingEventAttendance() - gets an attendance list for a given event ID (MEETS-internal ID)
     *
     * @param $id
     *
     * @return array
     */
    public function getConferencingEventAttendance($id) {
        return $this->apiCall('get_conferencing_event_attendance', 'get', ['meetsIdConferencingEvent' => $id]);
    }

    /**
     * downloadConferenceRecording() - gets a conference recording for a given recording ID (MEETS-internal ID)
     *
     * @param $id
     *
     * @return array
     */
    public function downloadConferenceRecording($id) {
        return $this->apiCall('download_conference_recording', 'post', ['meetsIdConferenceRecording' => $id], true);
    }
}