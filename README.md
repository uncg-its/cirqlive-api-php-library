# CirQlive API PHP Library

Contact: matt.libera@uncg.edu

# Introduction

This package is a PHP library used to interact with the CirQlive API.

> This is a **work in progress**. Not recommend for production apps just yet.

## Scope

This package is built not (yet) as a comprehensive interface with the CirqLive API - it is not written to perform every conceivable API call to CirqLive. Specifically, this package was built to suit the needs of UNC Greensboro's WebEx-CirqLive integration, and performs only the functions necessary for day-to-day operations of this configuration at UNCG.

Please fork and add methods to this as you see fit / as your needs dictate.

# Installation

1. `composer require 'uncgits/cirqlive-api-php-library'`
2. Use one of the API calls built into the `Uncgits\Cirqlive\CirqliveApi` class, or extend it and add your own. The standard API call is built in there. You'll have to find a way to call the setter methods to set the variables required to make the call - specific to your environment (e.g. urls, credentials, etc.)

# Version History

## 0.2

- Changes to PSR-4 since we were following it already
- disables exceptions on HTTP errors
- fix some references to Webex API

## 0.1.1
Adds support for HTTP Digest authentication. Other auth methods (OAuth, AWS, CirQlive) will be future adds.

It is confirmed right now that the `download_conference_recording` endpoint is currently broken. Theoretically the code supports it, however we have not been able to test with a real file since the current state of the endpoint returns either HTML code or a 404 error.

## 0.1

First real "release" (still beta). Available functionality:

- supports HTTP Basic authentication only.
- Endpoints available:
    - list_connections
    - list_courses
    - list_conferencing_events
    - list_conference_recordings
    - get_conferencing_event_attendance
- Still WIP:
    - download_conference_recording
